#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

void usage(string error){
    cout << error <<".\nUsage: pyramid.exe <height|int>" << endl;
}

int main(int argc, char **argv) {
    int size;
    if (argc != 2) {
        usage("Invalid number of arguments");
        exit(1);
    }
    try{
        size=stoi(argv[1]);
    }
    catch(std::exception& e) {
        usage("Invalid value for height");
        exit(2);
    }

    int width = size*2;
    string small;
    int i, j;
    for (i=0; i < width; i+=2)
    {
        string space((width-i)/2, ' ');
        string center(i, '_');
        small = "";

        cout << space << '/'<<  center << '\\'<< space << endl;
    }
}