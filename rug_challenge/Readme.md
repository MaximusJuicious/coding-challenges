**Premise**:

Programmatically create a rug of the provided size.
Let the size be passed in as a parameter.

**Tech Used**: C++

**Example**:<br>
~~~4~~~<br>
\~~434~~<br>
~43234~<br>
4321234<br>
~43234~<br>
\~~434~~<br>
~~~4~~~<br>
