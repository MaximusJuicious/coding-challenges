#include <iostream>
#include <string>

using namespace std;

void usage(string error){
    cout << error <<".\nUsage: rug.exe <size|int>" << endl;
}
void shrinking(int);
void expanding(int);

int main(int argc, char **argv) {
    int size;
    if (argc != 2) {
        usage("Invalid number of arguments");
        exit(1);
    }
    
    try{
        size=stoi(argv[1]);
    }
    catch(exception& e) {
        usage("Invalid value for size");
        exit(2);
    }

    shrinking(size);
    cout << "\n\n";
    expanding(size);
}

void shrinking(int size)
{    
    int largest = size/2;
    int current = largest;
    
    for (current=largest; current >= -largest; current--){
        int tmp_current = current;
        if (tmp_current < 0)
            tmp_current*=-1;
        string outer(tmp_current, '~');
        string inner;
        inner = char(largest-tmp_current+'a');
        int tmp = largest-tmp_current;
        while (tmp > 0){
            tmp--;
            inner = char(tmp+'a') + inner + char(tmp+'a');
        }
        cout << outer << inner << outer << endl;
    }
}   

void expanding(int size){
    string full = "";
    int largest = size/2;
    int current = largest;
    for (current=0; current<=largest; current++) {
        string outer(current, '~');
        string inner;
        inner = char(current+'a');
        int tmp = 0;
        while (tmp < (current-largest)*-1){
            tmp++;
            inner = char(tmp+'a') + inner + char(tmp+'a');
        }
        full = outer + inner + outer + "\n" + full;
        if (current != 0){
            full = full + outer + inner + outer + "\n";
        }
    }

    cout << full << endl;
}